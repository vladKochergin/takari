//
//  Example2Example2ConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class Example2ModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = Example2ViewControllerMock()
        let configurator = Example2ModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "Example2ViewController is nil after configuration")
        XCTAssertTrue(viewController.output is Example2Presenter, "output is not Example2Presenter")

        let presenter: Example2Presenter = viewController.output as! Example2Presenter
        XCTAssertNotNil(presenter.view, "view in Example2Presenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in Example2Presenter is nil after configuration")
        XCTAssertTrue(presenter.router is Example2Router, "router is not Example2Router")

        let interactor: Example2Interactor = presenter.interactor as! Example2Interactor
        XCTAssertNotNil(interactor.output, "output in Example2Interactor is nil after configuration")
    }

    class Example2ViewControllerMock: Example2ViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
