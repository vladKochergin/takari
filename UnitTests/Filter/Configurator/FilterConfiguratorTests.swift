//
//  FilterFilterConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class FilterModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = FilterViewControllerMock()
        let configurator = FilterModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "FilterViewController is nil after configuration")
        XCTAssertTrue(viewController.output is FilterPresenter, "output is not FilterPresenter")

        let presenter: FilterPresenter = viewController.output as! FilterPresenter
        XCTAssertNotNil(presenter.view, "view in FilterPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in FilterPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is FilterRouter, "router is not FilterRouter")

        let interactor: FilterInteractor = presenter.interactor as! FilterInteractor
        XCTAssertNotNil(interactor.output, "output in FilterInteractor is nil after configuration")
    }

    class FilterViewControllerMock: FilterViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
