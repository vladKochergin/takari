//
//  CatalogCatalogConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class CatalogModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = CatalogViewControllerMock()
        let configurator = CatalogModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "CatalogViewController is nil after configuration")
        XCTAssertTrue(viewController.output is CatalogPresenter, "output is not CatalogPresenter")

        let presenter: CatalogPresenter = viewController.output as! CatalogPresenter
        XCTAssertNotNil(presenter.view, "view in CatalogPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in CatalogPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is CatalogRouter, "router is not CatalogRouter")

        let interactor: CatalogInteractor = presenter.interactor as! CatalogInteractor
        XCTAssertNotNil(interactor.output, "output in CatalogInteractor is nil after configuration")
    }

    class CatalogViewControllerMock: CatalogViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
