//
//  ItemDetailsItemDetailsConfiguratorTests.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class ItemDetailsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = ItemDetailsViewControllerMock()
        let configurator = ItemDetailsModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ItemDetailsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ItemDetailsPresenter, "output is not ItemDetailsPresenter")

        let presenter: ItemDetailsPresenter = viewController.output as! ItemDetailsPresenter
        XCTAssertNotNil(presenter.view, "view in ItemDetailsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ItemDetailsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ItemDetailsRouter, "router is not ItemDetailsRouter")

        let interactor: ItemDetailsInteractor = presenter.interactor as! ItemDetailsInteractor
        XCTAssertNotNil(interactor.output, "output in ItemDetailsInteractor is nil after configuration")
    }

    class ItemDetailsViewControllerMock: ItemDetailsViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
