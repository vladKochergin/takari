//
//  Example1Example1PresenterTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class Example1PresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: Example1InteractorInput {

    }

    class MockRouter: Example1RouterInput {

    }

    class MockViewController: Example1ViewInput {

        func setupInitialState() {

        }
    }
}
