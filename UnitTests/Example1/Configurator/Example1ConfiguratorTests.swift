//
//  Example1Example1ConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class Example1ModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = Example1ViewControllerMock()
        let configurator = Example1ModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "Example1ViewController is nil after configuration")
        XCTAssertTrue(viewController.output is Example1Presenter, "output is not Example1Presenter")

        let presenter: Example1Presenter = viewController.output as! Example1Presenter
        XCTAssertNotNil(presenter.view, "view in Example1Presenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in Example1Presenter is nil after configuration")
        XCTAssertTrue(presenter.router is Example1Router, "router is not Example1Router")

        let interactor: Example1Interactor = presenter.interactor as! Example1Interactor
        XCTAssertNotNil(interactor.output, "output in Example1Interactor is nil after configuration")
    }

    class Example1ViewControllerMock: Example1ViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
