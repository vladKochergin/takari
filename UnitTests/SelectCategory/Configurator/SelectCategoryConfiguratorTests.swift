//
//  SelectCategorySelectCategoryConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class SelectCategoryModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = SelectCategoryViewControllerMock()
        let configurator = SelectCategoryModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "SelectCategoryViewController is nil after configuration")
        XCTAssertTrue(viewController.output is SelectCategoryPresenter, "output is not SelectCategoryPresenter")

        let presenter: SelectCategoryPresenter = viewController.output as! SelectCategoryPresenter
        XCTAssertNotNil(presenter.view, "view in SelectCategoryPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in SelectCategoryPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is SelectCategoryRouter, "router is not SelectCategoryRouter")

        let interactor: SelectCategoryInteractor = presenter.interactor as! SelectCategoryInteractor
        XCTAssertNotNil(interactor.output, "output in SelectCategoryInteractor is nil after configuration")
    }

    class SelectCategoryViewControllerMock: SelectCategoryViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
