//
//  SelectRegionSelectRegionConfiguratorTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class SelectRegionModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = SelectRegionViewControllerMock()
        let configurator = SelectRegionModuleConfigurator()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "SelectRegionViewController is nil after configuration")
        XCTAssertTrue(viewController.output is SelectRegionPresenter, "output is not SelectRegionPresenter")

        let presenter: SelectRegionPresenter = viewController.output as! SelectRegionPresenter
        XCTAssertNotNil(presenter.view, "view in SelectRegionPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in SelectRegionPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is SelectRegionRouter, "router is not SelectRegionRouter")

        let interactor: SelectRegionInteractor = presenter.interactor as! SelectRegionInteractor
        XCTAssertNotNil(interactor.output, "output in SelectRegionInteractor is nil after configuration")
    }

    class SelectRegionViewControllerMock: SelectRegionViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
