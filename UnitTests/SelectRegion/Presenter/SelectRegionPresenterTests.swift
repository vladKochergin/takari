//
//  SelectRegionSelectRegionPresenterTests.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import XCTest

class SelectRegionPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: SelectRegionInteractorInput {

    }

    class MockRouter: SelectRegionRouterInput {

    }

    class MockViewController: SelectRegionViewInput {

        func setupInitialState() {

        }
    }
}
