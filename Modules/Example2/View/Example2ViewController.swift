//
//  Example2Example2ViewController.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class Example2ViewController: UIViewController, Example2ViewInput, UITableViewDataSource, UITableViewDelegate {

    var output: Example2ViewOutput!
    var modelsArray: Array<ItemModel> = []
    
    @IBOutlet weak var tableView: UITableView!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: Example2ViewInput
    func setupInitialState() {
        self.tableView.register(UINib(nibName: "UITableViewItemCell", bundle: nil), forCellReuseIdentifier: "itemCell")
        let param:[String:[String:String]] = ["items_list" : ["page" : "0", "onpage" : "10i", "sort" : "publicated_less"]]
        RequestMapManager.getCatalogLists(parameters: param) { (array, dict, object) in
            self.modelsArray = array! as! Array<ItemModel>
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.modelsArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! UITableViewItemCell
        let model = self.modelsArray[indexPath.row]
        cell.setModel(model)
        
        return cell
    }
}
