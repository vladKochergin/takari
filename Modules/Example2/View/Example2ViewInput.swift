//
//  Example2Example2ViewInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol Example2ViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setupInitialState()
}
