//
//  Example2Example2ViewOutput.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol Example2ViewOutput {

    /**
        @author Vlad Kochergin
        Notify presenter that view is ready
    */

    func viewIsReady()
}
