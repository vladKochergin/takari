//
//  Example2Example2Initializer.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class Example2ModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var example2ViewController: Example2ViewController!

    override func awakeFromNib() {

        let configurator = Example2ModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: example2ViewController)
    }

}
