//
//  Example2Example2Configurator.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class Example2ModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? Example2ViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: Example2ViewController) {

        let router = Example2Router()

        let presenter = Example2Presenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = Example2Interactor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
