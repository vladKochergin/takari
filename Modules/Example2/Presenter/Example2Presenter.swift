//
//  Example2Example2Presenter.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class Example2Presenter: NSObject, Example2ModuleInput, Example2ViewOutput, Example2InteractorOutput {

    weak var view: Example2ViewInput!
    var interactor: Example2InteractorInput!
    var router: Example2RouterInput!

    func testRouter(){
        print("Test Router")
    }
    
    func viewIsReady() {
        self.view.setupInitialState()
    }
}
