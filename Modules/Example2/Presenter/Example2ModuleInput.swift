//
//  Example2Example2ModuleInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 14/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

protocol Example2ModuleInput: RamblerViperModuleInput {

    func testRouter()
}
