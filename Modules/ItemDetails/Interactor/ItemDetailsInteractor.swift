//
//  ItemDetailsItemDetailsInteractor.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//
import SwiftyJSON
class ItemDetailsInteractor: ItemDetailsInteractorInput {
    weak var output: ItemDetailsInteractorOutput!

    fileprivate var details: ItemDetails!{
        didSet{
            self.output.showDetails(details)
        }
    }
    
    func getDetailsForItem(with index: Int) {
        RequestMapManager.getDetailItem(itemID: index) { (array, dict, object) in
            self.details =  object as! ItemDetails
        }
    }
}
