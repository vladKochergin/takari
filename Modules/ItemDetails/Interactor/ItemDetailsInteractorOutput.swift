//
//  ItemDetailsItemDetailsInteractorOutput.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation

protocol ItemDetailsInteractorOutput: class {
    func showDetails(_ itemDetails: ItemDetails)
}
