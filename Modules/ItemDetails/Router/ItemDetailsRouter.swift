//
//  ItemDetailsItemDetailsRouter.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

class ItemDetailsRouter: ItemDetailsRouterInput {
   weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    func close() {
        self.transitionHandler.closeCurrentModule!(false)
    }
}
