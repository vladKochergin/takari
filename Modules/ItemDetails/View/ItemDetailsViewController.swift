//
//  ItemDetailsItemDetailsViewController.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import MBProgressHUD
import AMScrollingNavbar

class ItemDetailsViewController: UIViewController, ItemDetailsViewInput,UIGestureRecognizerDelegate {
    var expanded:Bool = false
    var expandedIndexPaths:[IndexPath] = []
    var descriptionCellIndexPaths:IndexPath?
    var output: ItemDetailsViewOutput!
    var items = [DetailViewModelItem]()
    
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextFieldX!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillDisappear(animated)
    }
    // MARK: Actions
    @IBAction func sendMessage(_ sender: Any) {
        
    }
    
    @IBAction func toggleFavorites(_ sender: Any) {
        print("star")
    }
    
    @IBAction func call(_ sender: UIButton) {
        print("call")
    }
    
    @IBAction func close(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
       // self.output.close()
    }
    
    // MARK: ItemDetailsViewInput
    func setupInitialState() {
        tableView.tableFooterView = UIView()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.navBarView.alpha = 0
        registerNIBs()
        tableView.estimatedRowHeight = 400.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    func showItem(_ item: DetailViewModel) {
        MBProgressHUD.hide(for: self.view, animated: true)
        items = item.items
        tableView.reloadData()
    }
    
    //MARK: Funcs
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    private func registerNIBs(){
        tableView.register(UINib(nibName: "DetailImageTableViewCell", bundle: nil), forCellReuseIdentifier: "imageCell")
        tableView.register(UINib(nibName: "SimilarAdsTableViewCell", bundle: nil), forCellReuseIdentifier: "similarAdCell")
        tableView.register(UINib(nibName: "DetailDescriptionTableViewCell", bundle: nil), forCellReuseIdentifier: "descriptionCell")
        tableView.register(UINib(nibName: "InternalsTableViewCell", bundle: nil), forCellReuseIdentifier: "internalsCell")
        tableView.register(UINib(nibName: "ViewsTableViewCell", bundle: nil), forCellReuseIdentifier: "viewsCell")
        tableView.register(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "mapCell")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y <= 100.0{
            let percent = (scrollView.contentOffset.y/150.0)
            self.navBarView.alpha = percent
        }else if(scrollView.contentOffset.y > 100){
            self.navBarView.alpha = 0.85
        }else if (scrollView.contentOffset.y < 60){
            self.navBarView.alpha = 0
        }
    }
    
    deinit {
        print("deinitialised")
    }
}

extension ItemDetailsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 300
        }
        if indexPath.row == descriptionCellIndexPaths?.row{
            return expandedIndexPaths.contains(indexPath) ? UITableViewAutomaticDimension:100
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        switch item.type {
        case .image:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as? DetailImageTableViewCell{
                cell.item = item
                return cell
            }
        case .internals:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "internalsCell", for: indexPath) as? InternalsTableViewCell{
                cell.item = item
                return cell
            }
        case .map:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "mapCell", for: indexPath) as? MapTableViewCell{
                cell.item = item
                return cell
            }
        case .desctription:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionCell", for: indexPath) as? DetailDescriptionTableViewCell{
                
                cell.item = item
                cell.buttonDelegate = self
                descriptionCellIndexPaths = indexPath
                cell.expanded = expanded
                return cell
            }
        case .similarAd:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "similarAdCell", for: indexPath) as? SimilarAdsTableViewCell{
                cell.item = item
                return cell
            }
            
        case .views:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "viewsCell", for: indexPath) as? ViewsTableViewCell{
                cell.item = item
                return cell
            }
        }
        return UITableViewCell()
    }
    
}
extension ItemDetailsViewController: ButtonCellDelegate{
    func cellTapped(cell: DetailDescriptionTableViewCell) {
        expanded = !expanded
        let indexPath = tableView.indexPath(for: cell)
        if expandedIndexPaths.contains(indexPath!){
            expandedIndexPaths.remove(at: expandedIndexPaths.index(of: indexPath!)!)
        }else{
            expandedIndexPaths.append(indexPath!)
        }
        tableView.reloadRows(at: [indexPath!], with: .fade)
    }
    
    
}
