//
//  ItemDetailsItemDetailsViewInput.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol ItemDetailsViewInput: class {

    /**
        @author parabellum199316
        Setup initial state of the view
    */

    func setupInitialState()
    func showItem(_ item:DetailViewModel)
}
