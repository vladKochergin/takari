//
//  ItemDetailsItemDetailsViewOutput.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol ItemDetailsViewOutput {

    /**
        @author parabellum199316
        Notify presenter that view is ready
    */

    func viewIsReady()
    func close()
}
