//
//  InternalsTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class InternalsTableViewCell: UITableViewCell {
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelInternalsItem else{
                return
            }
            self.nameLabel.text = item.internals.name
            self.valueLabel.text = item.internals.value
            
        }
    }
    // MARK: Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
}
