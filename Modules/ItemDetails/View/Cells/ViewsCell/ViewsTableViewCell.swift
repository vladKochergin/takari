//
//  ViewsTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/21/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class ViewsTableViewCell: UITableViewCell {
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelViewsItem else{
                return
            }
            self.idLabel.text = String(item.adID)
            self.numberOfViews.text = String(item.numberOfView)
        }
    }
    //MARK: Outlets
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var numberOfViews: UILabel!
    @IBOutlet weak var complainButton: UIButton!
    
    //MARK: Lyfe cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        //flip button image
        complainButton.semanticContentAttribute = .forceRightToLeft
    }

    
}
