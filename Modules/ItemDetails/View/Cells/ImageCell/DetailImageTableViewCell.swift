//
//  DetailImageTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import ImageSlideshow
//import SDWebImageSource

class DetailImageTableViewCell: UITableViewCell {
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelImageItem else{
                return
            }
            self.imageURLs = item.pictureURLs
            self.dateCreatedLabel.text = getFormattedDate(from: item.dateCreated)
            self.titleLabel.text = item.title
            self.priceLabel.text = "    " + item.priceDisplay + "   "
           
        }
    }
    var imageURLs:[URL] = []{
        didSet{
            //TODO: - not to fordet to change
            let imageSource = imageURLs.map { SDWebImageSource(urlString:$0.absoluteString)!}
            pageControll.numberOfPages = imageSource.count
            self.slider.setImageInputs(testImageSource)// TODO: - Change to imageSource
        }
    }
     var testImageSource = [SDWebImageSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!,SDWebImageSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, SDWebImageSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    // MARK: Outlets
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateCreatedLabel: UILabel!
    @IBOutlet weak var slider: ImageSlideshow!
    
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupImageSlider()
        
    }
    
    // MARK: Funcs
    private func getFormattedDate(from string:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myDate = dateFormatter.date(from: string)!
        dateFormatter.dateFormat = "dd MMMM, HH:mm"
        return dateFormatter.string(from: myDate)
    }
    
    fileprivate func setupImageSlider() {
        slider.backgroundColor = UIColor.white
        slider.slideshowInterval = 0
        slider.pageControlPosition = PageControlPosition.hidden
        slider.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        slider.pageControl.pageIndicatorTintColor = UIColor.black
        slider.contentScaleMode = UIViewContentMode.scaleAspectFill
        slider.activityIndicator = DefaultActivityIndicator()
        slider.currentPageChanged = {[weak self] page in
            self?.pageControll.currentPage = page
        }
    }
}
