//
//  MapTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import MapKit
class MapTableViewCell: UITableViewCell {
    private var location:CLLocation!
    private var localSearchRequest:MKLocalSearchRequest!
    private var localSearch:MKLocalSearch!
    private var foundCoordinates:CLLocationCoordinate2D!
    private let regionRadius:CLLocationDistance = 500
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelMapItem else{
                return
            }
            setupView(item)
        }
    }
    // MARK: Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var adsButton: UIButtonX!
    @IBOutlet weak var onlineStatusLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var cityName: UILabel!
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: Funcs
    fileprivate func setupView(_ item: DetailViewModelMapItem) {
        self.cityName.text = item.cityTitle
        self.userNameLabel.text = item.user.login
        if let latitude = item.detailLatitude, let longitude = item.detailLongitude{
            self.location = CLLocation(latitude: latitude, longitude: longitude)
            self.centerMapOnLocation(location: self.location)
        }else{
            findCity(cityName: item.cityTitle)
        }
    }
    private func centerMapOnLocation(location:CLLocation){
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func findCity(cityName:String){
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = cityName
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.start { (localResponse,error) in
            if localResponse == nil{
                return
            }
            self.foundCoordinates = CLLocationCoordinate2D(latitude: localResponse!.boundingRegion.center.latitude, longitude: localResponse!.boundingRegion.center.longitude)
            self.centerMapOnLocation(location: CLLocation(latitude: localResponse!.boundingRegion.center.latitude, longitude: localResponse!.boundingRegion.center.longitude))
        }
    }
}
