//
//  SimilarCollectionViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/25/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SimilarCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var title: UILabel!
    func setModel(_ model:ItemModel) {
        self.imageView?.sd_setImage(with: model.imageMedium, placeholderImage: UIImage(named: "image_placeholder"))
        self.title.text = model.title
        self.price.text = "   " + model.priceDisplay + "   "
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
