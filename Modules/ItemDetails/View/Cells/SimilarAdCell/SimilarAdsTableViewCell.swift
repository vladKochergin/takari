//
//  SimilarAdsTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SimilarAdsTableViewCell: UITableViewCell {
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelSimilarAdItem else{return}
            self.similarAdsModelItems = item.similarADs
        }
    }
    var similarAdsModelItems:[ItemModel] = []
        {
        didSet{
            collectionView.reloadData()
        }
    }
    //MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
   
    
    //MARL: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    // MARK:Funcs
    fileprivate func setupCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        
        self.collectionView.register(UINib(nibName: "SimilarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "similarCell")
    }
   
    deinit {
        collectionView.delegate = nil
        collectionView.dataSource = nil
    }
}

extension SimilarAdsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarAdsModelItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarCell", for: indexPath) as? SimilarCollectionViewCell
        let model = self.similarAdsModelItems[indexPath.row]
        itemCell?.setModel(model)
        return itemCell!
    }
    
}
extension SimilarAdsTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 3.0
        let height = width
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
