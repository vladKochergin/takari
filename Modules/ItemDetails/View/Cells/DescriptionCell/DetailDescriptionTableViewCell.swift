//
//  DetailDescriptionTableViewCell.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
protocol ButtonCellDelegate: class{
    func cellTapped(cell:DetailDescriptionTableViewCell)
}

class DetailDescriptionTableViewCell: UITableViewCell {
    weak var buttonDelegate:ButtonCellDelegate?
    var expanded:Bool = false{
        didSet{
            if expanded{
                self.moreLessButton.setImage(#imageLiteral(resourceName: "ic_strelka_less"), for: .normal)
            }else{
                self.moreLessButton.setImage(#imageLiteral(resourceName: "ic_strelka_more"), for: .normal)
            }
           
        }
    }
    var item:DetailViewModelItem?{
        didSet{
            guard let item = item as? DetailViewModelDescriptionItem else{
                return
            }
            let labelWidth = itemDescriptionLabel.frame.width
            let maxLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
            self.itemDescriptionLabel.text = item.detailDescription
            let actualLabelSize = itemDescriptionLabel.text!.boundingRect(with: maxLabelSize, options: [.usesLineFragmentOrigin], attributes: [NSFontAttributeName: itemDescriptionLabel.font], context: nil)
            let labelHeight = actualLabelSize.height
            if labelHeight < 60{
                hideExpandButton(true)
            }else{
                hideExpandButton(false)
            }
        }
    }
    private func hideExpandButton(_ shouldHide:Bool){
        if shouldHide{
            self.moreLessButton.isHidden = true
            self.separatorInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        }else{
            self.moreLessButton.isHidden = false
            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
        }
    
    }
    
    // MARK: Outlets
    @IBOutlet weak var moreLessButton: UIButton!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    
    // MARK: Actions
    @IBAction func moreLessButtonTapped(_ sender: Any) {
        if let delegate = buttonDelegate{
            delegate.cellTapped(cell: self)
        }
    }
    
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        //Set button image
        if expanded{
            self.moreLessButton.setImage(#imageLiteral(resourceName: "ic_strelka_less"), for: .normal)
        }else{
            self.moreLessButton.setImage(#imageLiteral(resourceName: "ic_strelka_more"), for: .normal)
        }
        
    }
}
