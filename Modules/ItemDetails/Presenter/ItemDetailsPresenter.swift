//
//  ItemDetailsItemDetailsPresenter.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//



class ItemDetailsPresenter: NSObject, ItemDetailsModuleInput, ItemDetailsViewOutput, ItemDetailsInteractorOutput {
    func close() {
        router.close()
    }
    
    func showDetails(_ itemDetails: ItemDetails) {
        self.item = DetailViewModel(with: itemDetails)
    }
    
    
    
    
    func showDetailsForItem(_ item: ItemModel) {
        self.interactor.getDetailsForItem(with: item.itemId!)
    }
    
    var item:DetailViewModel!{
        didSet{
            //Get item id and request details from interactor
            //Then view.show(item:ItemModel)
            view.showItem(item)
            
        }
    }
    weak var view: ItemDetailsViewInput!
    var interactor: ItemDetailsInteractorInput!
    var router: ItemDetailsRouterInput!
    
    func viewIsReady() {
        self.view.setupInitialState()
    }
}
