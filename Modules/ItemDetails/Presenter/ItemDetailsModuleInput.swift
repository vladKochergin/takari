//
//  ItemDetailsItemDetailsModuleInput.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

protocol ItemDetailsModuleInput: RamblerViperModuleInput {
    func showDetailsForItem(_ item:ItemModel)
    
}
