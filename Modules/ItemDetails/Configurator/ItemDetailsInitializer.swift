//
//  ItemDetailsItemDetailsInitializer.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class ItemDetailsModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var itemdetailsViewController: ItemDetailsViewController!

    override func awakeFromNib() {

        let configurator = ItemDetailsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: itemdetailsViewController)
    }

}
