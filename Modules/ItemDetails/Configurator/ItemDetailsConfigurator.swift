//
//  ItemDetailsItemDetailsConfigurator.swift
//  Takari
//
//  Created by parabellum199316 on 20/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class ItemDetailsModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? ItemDetailsViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: ItemDetailsViewController) {

        let router = ItemDetailsRouter()

        let presenter = ItemDetailsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ItemDetailsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
