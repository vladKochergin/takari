//
//  UICollectionViewItemCell.swift
//  Takari
//
//  Created by yvp on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class UICollectionViewItemCell: UICollectionViewCell {
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemAsap: UILabel!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemFixed: UIImageView!
    @IBOutlet weak var itemCategory: UILabel!
    @IBOutlet weak var itemLocation: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    func setModel(_ model:ItemModel) {
        self.itemImage?.sd_setImage(with: model.imageMedium, placeholderImage: UIImage(named: "image_placeholder"))
        
        self.itemTitle.text = model.title
        self.itemFixed.isHidden = !model.isFixed!
        self.itemCategory.text = model.categoryName
        self.itemLocation.text = model.cityName
        self.itemPrice.text = "   "+model.priceDisplay+"    "
        
        if model.isQuick! {
            // WARNING: Kostil
            self.itemAsap.text = "ASAP_ADV".localized
            self.itemTitle.text = "                   "+self.itemTitle.text!
        }
        
        if model.isPremium! {
            self.baseView.layer.borderWidth = 3.0
            self.baseView.layer.borderColor = UIColor(red: 248/255, green: 186/255, blue: 65/255, alpha: 1).cgColor
            self.baseView.backgroundColor = UIColor(red: 253/255, green: 243/255, blue: 224/255, alpha: 1)
            self.baseView.viewPath(distance: 60, width: 40, text: "PREMIUM_ADV".localized)
        }
    }
    
    override func prepareForReuse() {
        self.baseView.removeLabelView()
        self.baseView.layer.borderColor = UIColor.clear.cgColor
        self.baseView.layer.borderWidth = 0
        self.baseView.backgroundColor = UIColor.white//UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
        self.favoriteButton.isSelected = false
        self.itemAsap.text = ""
        self.itemImage.image = UIImage()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.itemAsap.layer.cornerRadius = 5
        self.itemAsap.layer.masksToBounds = true
        self.backgroundColor = UIColor.white//UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1) //238
    }
    
    // MARK: Action
    
    @IBAction func favoriteButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if(sender.isSelected){
            //Add to favorite
        }else{
            //Remove from favorite
        }
    }
}
