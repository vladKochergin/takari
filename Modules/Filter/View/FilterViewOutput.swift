//
//  FilterFilterViewOutput.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol FilterViewOutput {

    /**
        @author Vlad Kochergin
        Notify presenter that view is ready
    */

    func viewIsReady()
    func showSelectCategory()
    func getDataSourceWithFilter(filter: FilterModel)
}
