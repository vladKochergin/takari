//
//  SortedCell.swift
//  Takari
//
//  Created by yvp on 9/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SortedCell: UITableViewCell {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var label: UILabel!
    
    var output: callBack? = nil
    var item: FilterCellItemSorted? = nil
    
    func setupCell(item: FilterCellItemSorted) {
        self.item = item
        self.label.text = "Сортировка"
        for (index, element) in item.parameters.enumerated() {
            self.segmentControl.setTitle(element, forSegmentAt: index)
        }
        self.segmentControl.selectedSegmentIndex = item.sortedType.hashValue
    }
    
    @IBAction func segmentChangeValue(_ sender: UISegmentedControl) {
        self.item?.sortedType = FilterSortedType.allValue[sender.selectedSegmentIndex]
        self.output?.callBack(items: self.item!)
    }    
}
