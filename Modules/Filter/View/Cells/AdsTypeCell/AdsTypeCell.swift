//
//  AdsTypeCell.swift
//  Takari
//
//  Created by yvp on 9/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class AdsTypeCell: UITableViewCell {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var label: UILabel!
    
    var output: callBack? = nil
    var item: FilterCellItemAdsType? = nil
    
    func setupCell(item: FilterCellItemAdsType) {
        self.item = item
        self.label.text = "Частные или бизнес"
        for (index, element) in item.parameters.enumerated() {
            self.segmentControl.setTitle(element, forSegmentAt: index)
        }
        self.segmentControl.selectedSegmentIndex = item.adsType.hashValue
    }
    
    @IBAction func segmentChangeValue(_ sender: UISegmentedControl) {
        self.item?.adsType = FilterAdsType.allValue[sender.selectedSegmentIndex]
        self.output?.callBack(items: self.item!)
    }
    
}
