//
//  SearchCell.swift
//  Takari
//
//  Created by yvp on 9/26/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var searchTextField: UITextField!
    
    var output: callBack? = nil
    var item: FilterCellItemSearch? = nil
    
    func setupCell(item: FilterCellItemSearch) {
        self.item = item
        self.searchTextField.text = item.searchText
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func endEditingTextField(_ sender: UITextField) {
        self.item?.searchText = sender.text!
        self.output?.callBack(items: self.item!)
    }
    
}
