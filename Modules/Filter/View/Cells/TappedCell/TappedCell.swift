//
//  TappedCell.swift
//  Takari
//
//  Created by yvp on 9/26/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class TappedCell: UITableViewCell {
    
    var output: callBack? = nil
    var item: FilterCellItems? = nil
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var descriptionCell: UILabel!
    @IBOutlet weak var cellLabel: UILabel!
    
    func setupLocationCell(item: FilterCellItemLocation) {
        self.cellImage.image = item.image
        //WARNINP: LocationModel
        self.cellLabel.text = "Выбрать город"
    }
    
    func setupCategoryCell(item: FilterCellItemCategory) {
        self.cellLabel.text = "Выбрать рубрику"
        self.cellImage.image = item.image
        self.item = item
        if item.categoryModel.categoryId != 0 {
            self.cellLabel.text = item.categoryModel.title
            self.descriptionCell.text = "Рубрика"
        }
    }
    
    //    func setupCell(text: String, image: UIImage) {
    //        self.cellImage.image = image
    //        self.cellLabel.text = text
    //    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.output?.callBack(items: self.item!)
        }
        // Configure the view for the selected state
    }
    
}
