//
//  FilterFilterViewController.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

protocol callBack: class {
    func callBack(items: FilterCellItems)
}

class FilterViewController: UITableViewController, FilterViewInput, callBack, SelectCategoryDelegate {
    
    var output: FilterViewOutput!
    var dataSource: FilterDataSource!
    var filter: FilterModel!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    
    // MARK: FilterViewInput
    func setupInitialState() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        self.tableView.register(UINib(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        self.tableView.register(UINib(nibName: "TappedCell", bundle: nil), forCellReuseIdentifier: "tappedCell")
        self.tableView.register(UINib(nibName: "SortedCell", bundle: nil), forCellReuseIdentifier: "sortedCell")
        self.tableView.register(UINib(nibName: "AdsTypeCell", bundle: nil), forCellReuseIdentifier: "adsTypeCell")
    }
    
    func set(filter: FilterModel) {
        self.filter = filter
    }
    
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func reloadDataWith(filterDataSource: FilterDataSource) {
        self.dataSource = filterDataSource
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.items.keys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.items[section.description]!.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.dataSource.items[indexPath.section.description]![indexPath.row]
        
        switch item.type {
        case .search:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as? SearchCell{
                cell.setupCell(item: item as! FilterCellItemSearch)
                cell.output = self
                return cell
            }
        case .location:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "tappedCell", for: indexPath) as? TappedCell{
                cell.setupLocationCell(item: item as! FilterCellItemLocation)
                return cell
            }
        case .category:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "tappedCell", for: indexPath) as? TappedCell{
                cell.setupCategoryCell(item: item as! FilterCellItemCategory)
                cell.output = self
                return cell
            }
        case .sorted:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "sortedCell", for: indexPath) as? SortedCell{
                cell.setupCell(item: item as! FilterCellItemSorted)
                cell.output = self
                return cell
            }
        case .internals:
            return UITableViewCell()
        case .adsType:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "adsTypeCell", for: indexPath) as? AdsTypeCell{
                cell.setupCell(item: item as! FilterCellItemAdsType)
                cell.output = self
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func callBack(items: FilterCellItems) {
        switch items.type {
        case .sorted:
            self.filter.sorted = (items as! FilterCellItemSorted).sortedType
        case .adsType:
            self.filter.adsType = (items as! FilterCellItemAdsType).adsType
        case .search:
            self.filter.searchText = (items as! FilterCellItemSearch).searchText
        case .category:
            self.output.showSelectCategory()
        default:
            return
        }
    }
    
    //MARK: SelectCategoryDelegate
    func didEndSelect(selectView: SelectCategoryViewController, categoryModel: CategoryModel){
        self.filter.category = categoryModel
        self.output.getDataSourceWithFilter(filter: filter)
    }
}
