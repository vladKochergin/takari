//
//  FilterCellItems.swift
//  Takari
//
//  Created by yvp on 9/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

protocol FilterCellItems: class{
    var type: FilterCellItemType {get}
    var data: FilterCellItems {get}
}

//class FilterCellItems: NSObject {
//
//}

enum FilterCellItemType{
    case search
    case location
    case category
    case internals
    case sorted
    case adsType
}

class FilterCellItemSearch: FilterCellItems{
    var type: FilterCellItemType{
        return .search
    }
    var data: FilterCellItems{
        return self
    }
    
    var searchText:String
    
    init(searchText: String) {
        self.searchText = searchText
    }
}

class FilterCellItemLocation: FilterCellItems{
    var type:FilterCellItemType{
        return .location
    }
    var data: FilterCellItems{
        return self
    }
    
    var locationModel: AnyObject
    var image: UIImage
    
    init(locationModel: AnyObject, image: UIImage){
        self.locationModel = locationModel
        self.image = image
    }
}

class FilterCellItemCategory: FilterCellItems{
    var type: FilterCellItemType{
        return .category
    }
    var data: FilterCellItems{
        return self
    }
    
    var categoryModel: CategoryModel
    var image: UIImage
    
    init(categoryModel: CategoryModel, image: UIImage) {
        self.categoryModel = categoryModel
        self.image = image
    }
}

//class FilterCellItemInternals: DetailViewModelItem{
//    var type: FilterCellItemType{
//        return .internals
//    }
//    var internals: Internals
//    init(internals:Internals) {
//        self.internals = internals
//    }
//}

class FilterCellItemSorted: FilterCellItems{
    var type: FilterCellItemType{
        return .sorted
    }
    var data: FilterCellItems{
        return self
    }
    
    var sortedType: FilterSortedType
    var parameters: [String]
    var filterModel: FilterModel
    
    init(sortedType: FilterSortedType, parameters: [String], filter: FilterModel){
        self.sortedType = sortedType
        self.parameters = parameters
        self.filterModel = filter
    }
}

class FilterCellItemAdsType: FilterCellItems{
    var type: FilterCellItemType{
        return .adsType
    }
    var data: FilterCellItems{
        return self
    }
    
    var adsType: FilterAdsType
    var parameters: [String]
    
    init(adsType: FilterAdsType, parameters: [String]) {
        self.adsType = adsType
        self.parameters = parameters
    }
}


