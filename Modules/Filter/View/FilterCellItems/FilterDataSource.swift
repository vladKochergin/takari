//
//  FilterDataSource.swift
//  Takari
//
//  Created by yvp on 9/27/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class FilterDataSource: NSObject {

    var items: [String: [FilterCellItems]] = Dictionary()
    
    init(filter: FilterModel){
        //Search
        let searchCell = FilterCellItemSearch(searchText: filter.searchText)
        self.items["0"] = [searchCell]
        
        //Location
        let locationCell = FilterCellItemLocation(locationModel: AnyObject.self as AnyObject, image:UIImage(named:"ic_location_filters")!)
        //Category
        let categoryCell = FilterCellItemCategory(categoryModel:filter.category!, image:UIImage(named:"ic_categ_filters")!)
        self.items["1"] = [locationCell, categoryCell]
        
        //Internals
        self.items["2"] = []
        
        //Sorted
        let sortedCell = FilterCellItemSorted(sortedType: filter.sorted, parameters:["Самые новые", "Самые дешевые", "Самые дорогие"], filter:filter)
        //ADS type
        let adsTypeCell = FilterCellItemAdsType(adsType: filter.adsType, parameters:["Все", "Частное", "Бизнес"])
        self.items["3"] = [sortedCell, adsTypeCell]
    }
}
