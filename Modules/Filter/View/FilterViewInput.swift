//
//  FilterFilterViewInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol FilterViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setupInitialState()
    func reloadDataWith(filterDataSource: FilterDataSource)
    func set(filter: FilterModel)
}
