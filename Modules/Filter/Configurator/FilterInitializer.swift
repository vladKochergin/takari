//
//  FilterFilterInitializer.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class FilterModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var filterViewController: FilterViewController!

    override func awakeFromNib() {

        let configurator = FilterModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: filterViewController)
    }

}
