//
//  FilterFilterConfigurator.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class FilterModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? FilterViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: FilterViewController) {

        let router = FilterRouter()

        let presenter = FilterPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = FilterInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
