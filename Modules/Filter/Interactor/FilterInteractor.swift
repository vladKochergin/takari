//
//  FilterFilterInteractor.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class FilterInteractor: FilterInteractorInput {

    weak var output: FilterInteractorOutput!

    func getDataSourceWithFilter(filter: FilterModel){
        let filterItemsCell = FilterDataSource(filter: filter)
        self.output.reloadDataWith(filterDataSource: filterItemsCell)
    }
}
