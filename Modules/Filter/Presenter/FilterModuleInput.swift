//
//  FilterFilterModuleInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

protocol FilterModuleInput: RamblerViperModuleInput {

    func config(filter: FilterModel)
}
