//
//  FilterFilterPresenter.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class FilterPresenter: NSObject, FilterModuleInput, FilterViewOutput, FilterInteractorOutput {

    weak var view: FilterViewInput!
    var interactor: FilterInteractorInput!
    var router: FilterRouterInput!

    func config(filter: FilterModel) {
        self.interactor.getDataSourceWithFilter(filter: filter)
        self.view.set(filter: filter)
    }
    
    func getDataSourceWithFilter(filter: FilterModel) {
        self.interactor.getDataSourceWithFilter(filter: filter)
    }
    
    func viewIsReady() {
        self.view.setupInitialState()
    }
    
    func reloadDataWith(filterDataSource: FilterDataSource){
        self.view.reloadDataWith(filterDataSource: filterDataSource)
    }
    
    func showSelectCategory() {
        self.router.showSelectCategory()
    }
}
