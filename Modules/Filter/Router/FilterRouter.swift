//
//  FilterFilterRouter.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

class FilterRouter: FilterRouterInput {
    
    weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    func transitionModuleFactory(vcName: String) -> RamblerViperModuleFactoryProtocol {
        let factory = RamblerViperModuleFactory.init(storyboard: UIStoryboard(name: "Main", bundle: nil), andRestorationId: vcName)
        return factory!
    }
    
    func showSelectCategory() {
        transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "categoryViewController")) { (src, dst) in
            (dst as! SelectCategoryViewController).delegate = (src as! FilterViewController)
            (src as! UIViewController).navigationController?.pushViewController(dst as! UIViewController, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                guard (moduleInput as? SelectCategoryModuleInput) != nil else {
                    fatalError("invalid module type")
                }
                let module = moduleInput as! SelectCategoryModuleInput
                module.config(categoryLVL: 1, parentId: 1)
                return nil
        }
    }
}
