//
//  FilterFilterRouterInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 26/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation

protocol FilterRouterInput {

    func showSelectCategory()
    
}
