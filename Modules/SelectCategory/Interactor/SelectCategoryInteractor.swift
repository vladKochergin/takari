//
//  SelectCategorySelectCategoryInteractor.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class SelectCategoryInteractor: SelectCategoryInteractorInput {
    
    weak var output: SelectCategoryInteractorOutput!
    
    func categoryWith(lvl: Int, parentId: Int){
        if lvl > 1 {
            self.loadCategory(lvl: lvl, parentId: parentId)
            return
        }
        
        RequestMapManager.getCategoryTree { (array, dict, object) in
            self.loadCategory(lvl: lvl, parentId: parentId)
        }
    }
    
    func loadCategory(lvl: Int, parentId: Int) {
        let array = RealmService.getCategory(lvl: lvl, parentId: parentId)
        self.output.reloadCategory(array: array)
    }
}
