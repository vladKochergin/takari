//
//  SelectCategorySelectCategoryViewInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol SelectCategoryViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setCurrentCategory(category: CategoryModel)
    func setupInitialState()
    func reloadCategory(array: Array<CategoryModel>)
}
