//
//  SelectCategorySelectCategoryViewOutput.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol SelectCategoryViewOutput {

    /**
        @author Vlad Kochergin
        Notify presenter that view is ready
    */

    func viewIsReady()
    func showSubCategory(fromCategory: CategoryModel)
    func closeAllView()
}
