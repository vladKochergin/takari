//
//  SelectCategorySelectCategoryViewController.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol SelectCategoryDelegate: class {
    func didEndSelect(selectView: SelectCategoryViewController, categoryModel: CategoryModel)
}

class SelectCategoryViewController: UITableViewController, SelectCategoryViewInput {
    var output: SelectCategoryViewOutput!
    weak var delegate: SelectCategoryDelegate?
    var dataSource: Array<CategoryModel> = []
    var currentCategory: CategoryModel? = nil
    var allInCategory: String {
        get {
            if self.currentCategory != nil {
                return "ALL_IN_CATEGORY".localized + self.currentCategory!.title
            }
            return "ALL_IN_ALL_CATEGORY".localized
        }
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    // MARK: SelectCategoryViewInput
    func setupInitialState() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func setCurrentCategory(category: CategoryModel){
        self.currentCategory = category
        self.navigationItem.title = category.title
    }
    
    func reloadCategory(array: Array<CategoryModel>) {
        MBProgressHUD.hide(for: self.view, animated: true)
        self.dataSource = array
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            //WARNIN (First sec.)
            let model = self.currentCategory
            self.delegate?.didEndSelect(selectView: self, categoryModel: model!)
            return
        }
        
        let model = self.dataSource[indexPath.row-1]
        if(model.subsCount > 0) {
            self.output.showSubCategory(fromCategory: model)
            return
        }
        
        self.delegate?.didEndSelect(selectView: self, categoryModel: model)
        self.output.closeAllView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataSource.count == 0 {
            return 0
        }
        
        return self.dataSource.count+1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CategoryCell
        
        if indexPath.row == 0 {
            cell.categoryTitle.text = self.allInCategory
            cell.advCount.text = ""
            return cell
        }
        
        let model = self.dataSource[indexPath.row-1]
        if model.subsCount > 0 {
            cell.accessoryType = .disclosureIndicator
        }
        cell.advCount.text = "ADS".localized + model.itemsCount.description
        cell.categoryTitle.text = model.title
        if(model.lvl == 1) {
            cell.categoryImage.sd_setImage(with: URL(string: model.image), placeholderImage: UIImage(named: "image_placeholder"))
        }
        
        return cell
    }
}
