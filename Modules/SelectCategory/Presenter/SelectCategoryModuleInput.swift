//
//  SelectCategorySelectCategoryModuleInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

protocol SelectCategoryModuleInput: RamblerViperModuleInput {

    func config(categoryLVL: Int, parentId: Int)
    func config(categoryModel: CategoryModel)
}
