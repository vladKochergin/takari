//
//  SelectCategorySelectCategoryPresenter.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class SelectCategoryPresenter: NSObject, SelectCategoryModuleInput, SelectCategoryViewOutput, SelectCategoryInteractorOutput {
    weak var view: SelectCategoryViewInput!
    var interactor: SelectCategoryInteractorInput!
    var router: SelectCategoryRouterInput!

    func config(categoryLVL: Int, parentId: Int){
        self.interactor.categoryWith(lvl: categoryLVL, parentId: parentId)
    }
    
    func config(categoryModel: CategoryModel) {
        self.view.setCurrentCategory(category: categoryModel)
        self.interactor.categoryWith(lvl: categoryModel.lvl+1, parentId: categoryModel.categoryId)
    }
    
    func viewIsReady() {
        self.view.setupInitialState()
    }
    
    func reloadCategory(array: Array<CategoryModel>) {
        self.view.reloadCategory(array: array)
    }
    
    func showSubCategory(fromCategory: CategoryModel) {
        self.router.showSubCategory(fromCategory: fromCategory)
    }
    
    func closeAllView(){
        self.router.closeAllView()
    }
}
