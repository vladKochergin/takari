//
//  SelectCategorySelectCategoryRouter.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

class SelectCategoryRouter: SelectCategoryRouterInput {
   weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    func showSubCategory(fromCategory: CategoryModel?){
        self.transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "categoryViewController")) { (sourceModuleTransitionHandler, destinationModuleTransitionHandler) in
            let src = sourceModuleTransitionHandler as! SelectCategoryViewController
            let dst = destinationModuleTransitionHandler as! SelectCategoryViewController
            dst.delegate = src.delegate!
            src.navigationController?.pushViewController(dst, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                guard (moduleInput as? SelectCategoryModuleInput) != nil else {
                    fatalError("invalid module type")
                }
                let module = moduleInput as! SelectCategoryModuleInput
                if fromCategory != nil {
                    module.config(categoryModel: fromCategory!)
                } else{
                    module.config(categoryLVL: 1, parentId: 1)
                }
                
                return nil
        }
    }
    
    func closeAllView() {
        self.transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "catalogViewController")) { (sourceModuleTransitionHandler, destinationModuleTransitionHandler) in
            let src = sourceModuleTransitionHandler as! UIViewController
//            let dst = destinationModuleTransitionHandler as! UIViewController
            var newStack: Array<UIViewController> = []
            for view in (src.navigationController?.viewControllers)! {
                if !(view.isKind(of: SelectCategoryViewController.self)) {
                    newStack.append(view)
                }
            }
            src.navigationController?.setViewControllers(newStack, animated: true);
//            dst.navigationController?.setViewControllers(newStack, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                return nil
        }
    }
    
    func transitionModuleFactory(vcName: String) -> RamblerViperModuleFactoryProtocol {
        let factory = RamblerViperModuleFactory.init(storyboard: UIStoryboard(name: "Main", bundle: nil), andRestorationId: vcName)
        return factory!
    }
}
