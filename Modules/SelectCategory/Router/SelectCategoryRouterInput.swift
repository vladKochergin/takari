//
//  SelectCategorySelectCategoryRouterInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation

protocol SelectCategoryRouterInput {

    func showSubCategory(fromCategory: CategoryModel?)
    func closeAllView()
}
