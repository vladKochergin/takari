//
//  SelectCategorySelectCategoryInitializer.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SelectCategoryModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var selectcategoryViewController: SelectCategoryViewController!

    override func awakeFromNib() {

        let configurator = SelectCategoryModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: selectcategoryViewController)
    }

}
