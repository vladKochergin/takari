//
//  SelectCategorySelectCategoryConfigurator.swift
//  Takari
//
//  Created by Vlad Kochergin on 22/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SelectCategoryModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? SelectCategoryViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: SelectCategoryViewController) {

        let router = SelectCategoryRouter()

        let presenter = SelectCategoryPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SelectCategoryInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
