//
//  CatalogCatalogModuleInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

protocol CatalogModuleInput: RamblerViperModuleInput {

    func config(filter: FilterModel)
}
