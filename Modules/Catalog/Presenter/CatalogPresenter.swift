//
//  CatalogCatalogPresenter.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class CatalogPresenter: NSObject, CatalogModuleInput, CatalogViewOutput, CatalogInteractorOutput {
   
    

    weak var view: CatalogViewInput!
    var interactor: CatalogInteractorInput!
    var router: CatalogRouterInput!

    func config(filter: FilterModel) {
        self.view.setFilter(filter: filter)
        self.interactor.setFilter(filter: filter)
    }
    
    func viewIsReady() {
        self.view.setupInitialState()
    }
    
    func getData(loadNextPage nextPage:Bool!){
        self.interactor.getData(loadNextPage: nextPage)
    }
    
    func didSelectItem(_ item: ItemModel) {
        router.presentDetails(forItemModel: item)
    }
    
    func showCategoryView(){
        self.router.showCategoryView()
    }
    
    func showCatalogViewWithFilters(filter: FilterModel){
        self.router.showCatalogViewWithFilters(filter: filter)
    }
    
    func showFilterWith(filter: FilterModel) {
        self.router.showFilterWith(filter: filter)
    }
    
    // MARK: Interactor
    
    func reloadData(dataSource:Array<ItemModel>){
        self.view.reloadData(dataSource: dataSource)
    }
}
