//
//  CatalogCatalogInteractor.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class CatalogInteractor: CatalogInteractorInput {
    
    weak var output: CatalogInteractorOutput!
    
    fileprivate var dataSource: Array<ItemModel> = []
    fileprivate var filter:FilterModel?
    fileprivate var currentPage:Int! = -1
    fileprivate var load:Bool! = true
    
    func setFilter(filter: FilterModel){
        self.filter = filter
    }
    
    func getData(loadNextPage nextPage:Bool!){
        if !load {
            return
        }
        
        if nextPage {
            self.currentPage = self.currentPage + 1
        } else {
            self.currentPage = 0
            self.dataSource = []
            load = true
        }
        
        var param = ["page" : String(self.currentPage), "onpage" : "10i", "sort" : "publicated_less"]
        self.filter?.dictionary().forEach { (arg) in
            let (k, v) = arg
            param[k] = v
        }
        
        RequestMapManager.getCatalogLists(parameters: ["items_list" : param]) { (array, dict, object) in
            if array?.count == nil {
                self.load = false
            }
            self.dataSource.append(contentsOf: array as! [ItemModel])
            self.output.reloadData(dataSource: self.dataSource)
        }
    }
}
