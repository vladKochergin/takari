//
//  CatalogCatalogInteractorOutput.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation

protocol CatalogInteractorOutput: class {

    func reloadData(dataSource:Array<ItemModel>)
    
}
