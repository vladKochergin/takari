//
//  CatalogCatalogRouterInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation

protocol CatalogRouterInput {
    
    func presentDetails(forItemModel item: ItemModel)
    func showCategoryView()
    func showCatalogViewWithFilters(filter: FilterModel)
    func showFilterWith(filter: FilterModel)
}
