//
//  CatalogCatalogRouter.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import ViperMcFlurry

class CatalogRouter: CatalogRouterInput {
    weak var transitionHandler: RamblerViperModuleTransitionHandlerProtocol!
    
    func showCatalogViewWithFilters(filter: FilterModel){
        transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "catalogViewController")) { (src, dst) in
//            (dst as! SelectCategoryViewController).delegate = (src as! CatalogViewController)
            (src as! CatalogViewController).navigationController?.pushViewController(dst as! CatalogViewController, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                guard (moduleInput as? CatalogModuleInput) != nil else {
                    fatalError("invalid module type")
                }
                let module = moduleInput as! CatalogModuleInput
                module.config(filter: filter)
                return nil
        }
    }
    
    func showCategoryView(){
        transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "categoryViewController")) { (src, dst) in
            (dst as! SelectCategoryViewController).delegate = (src as! CatalogViewController)
            (src as! UIViewController).navigationController?.pushViewController(dst as! UIViewController, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                guard (moduleInput as? SelectCategoryModuleInput) != nil else {
                    fatalError("invalid module type")
                }
                let module = moduleInput as! SelectCategoryModuleInput
                module.config(categoryLVL: 1, parentId: 1)
                return nil
        }
    }
    
    func presentDetails(forItemModel item: ItemModel) {
        transitionHandler.openModule!(usingSegue:"ItemDetailsSegue").thenChain { (moduleInput) -> RamblerViperModuleOutput? in
            guard (moduleInput as? ItemDetailsModuleInput) != nil else {
                fatalError("invalid module type")
            }
            let module:ItemDetailsModuleInput = moduleInput as! ItemDetailsModuleInput
            module.showDetailsForItem(item)
            
            return nil
        }
    }
    
    func showFilterWith(filter: FilterModel) {
        transitionHandler.openModule!(usingFactory: self.transitionModuleFactory(vcName: "FilterViewController")) { (src, dst) in
            (src as! UIViewController).navigationController?.pushViewController(dst as! UIViewController, animated: true)
            }.thenChain { (moduleInput) -> RamblerViperModuleOutput? in
                guard (moduleInput as? FilterModuleInput) != nil else {
                    fatalError("invalid module type")
                }
                let module = moduleInput as! FilterModuleInput
                module.config(filter: filter)
                return nil
        }
    }
    
    func transitionModuleFactory(vcName: String) -> RamblerViperModuleFactoryProtocol {
        let factory = RamblerViperModuleFactory.init(storyboard: UIStoryboard(name: "Main", bundle: nil), andRestorationId: vcName)
        return factory!
    }
}
