//
//  ToolBar.swift
//  Takari
//
//  Created by yvp on 9/19/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class ToolBar: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var switchButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit(){
        self.backgroundColor = UIColor.white
        Bundle.main.loadNibNamed("ToolBar", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
