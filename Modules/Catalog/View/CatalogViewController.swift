
//
//  CatalogCatalogViewController.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class CatalogViewController: UIViewController, CatalogViewInput, SelectCategoryDelegate {
    enum ViewType {
        case tableView
        case collectionView
    }
    
    enum contrlollerType {
        case withFilert
        case standart
    }
    
    //Public properties
    var scrollingDelay:Double = 50
    var output: CatalogViewOutput!
    var filter: FilterModel = FilterModel()
    var controlletType: contrlollerType = .standart
    var searchView: SearchView = SearchView()
    
    //Private properties
    fileprivate var dataSource: Array<ItemModel> = []
    fileprivate weak var navController:ScrollingNavigationController!
    var viewType:ViewType = .tableView{
        didSet {
            if oldValue == .tableView {
                viewType = .collectionView
                self.toggleView(from: tableView, to: collectionView)
            } else {
                viewType = .tableView
                self.toggleView(from: collectionView, to: tableView)
            }
        }
    }
    
    //Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var toolBarView: ToolBar!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let currentView = viewType == .tableView ? tableView : collectionView
        if tabBarController != nil {
            navController.followScrollView(currentView, delay: scrollingDelay, scrollSpeedFactor: 2, followers: [toolBarView, tabBarController!.tabBar])
        } else {
            navController.followScrollView(currentView, delay: scrollingDelay, scrollSpeedFactor: 2, followers: [toolBarView])
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navController.showNavbar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func setupInitialState() {
        setupViews()
        createSearchField()
        registerNIBs()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filters", style: .plain, target: self, action: #selector(showFilterView))
        self.getData(nextPage: false)
    }
    
    private func setupViews(){
        self.tableView.addRefreshControlWithSelector(selector: #selector(pullData), view: self)
        self.collectionView.addRefreshControlWithSelector(selector: #selector(pullData), view: self)
        self.collectionView.isHidden = true
        if let layout = collectionView?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navController = self.navigationController as? ScrollingNavigationController
        tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
        collectionView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
        self.toolBarView.switchButton.addTarget(self, action: #selector(switchViews(button:)), for: .touchUpInside)
        self.toolBarView.categoryButton.addTarget(self, action: #selector(showCategoryView), for: .touchUpInside)
    }
    
    private func registerNIBs(){
        self.tableView.register(UINib(nibName: "UITableViewItemCell", bundle: nil), forCellReuseIdentifier: "itemModelCell")
        self.collectionView.register(UINib(nibName: "UICollectionViewItemCell", bundle: nil), forCellWithReuseIdentifier: "itemModelCell")
    }
    
    private func createSearchField() {
        let frame: CGRect = (navigationController?.navigationBar.bounds)!
        self.searchView = SearchView(frame: frame)
        self.navigationItem.titleView = self.searchView
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        navController.showNavbar()
        return true
    }
    
    // MARK: Data
    
    func setFilter(filter: FilterModel) {
        self.filter = filter
        self.controlletType = .withFilert
    }
    
    func pullData(){
        self.getData(nextPage: false)
    }
    
    func getData(nextPage:Bool!){
        if nextPage {
            self.tableView.showPagingSpinner()
        }
        self.output.getData(loadNextPage: nextPage)
    }
    
    func reloadData(dataSource:Array<ItemModel>){
        self.dataSource = dataSource
        if self.viewType == .tableView{
            self.tableView.reloadDataWithCheck()
        }else{
            self.collectionView.reloadData()
        }
    }
    
    // MARK: Actions
    
    func showFilterView() {
        self.output.showFilterWith(filter: self.filter)
    }
    
    func switchViews(button: UIButton) {
        if viewType == .tableView{
            button.setImage(UIImage(named: "ic_list"), for: .normal)
            viewType = .collectionView
        }else{
            button.setImage(UIImage(named: "ic_grid"), for: .normal)
            viewType = .tableView
        }
    }
    func showCategoryView() {
        self.output.showCategoryView()
    }
    
    private func toggleView(from: UIView, to: UIView){
        from.isHidden = true
        to.isHidden = false
        if to.isKind(of: UICollectionView.self) {
            self.collectionView.reloadData()
        } else {
            self.tableView.reloadDataWithCheck()
        }
        if tabBarController != nil {
            navController.followScrollView(to, delay: scrollingDelay, scrollSpeedFactor: 1, followers: [toolBarView, tabBarController!.tabBar])
        } else {
            navController.followScrollView(to, delay: scrollingDelay, scrollSpeedFactor: 1, followers: [toolBarView])
        }
    }
    
    func didEndSelect(selectView: SelectCategoryViewController, categoryModel: CategoryModel){
        self.filter.category = categoryModel
        self.output.showCatalogViewWithFilters(filter: filter)
    }
}

extension CatalogViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if viewType == .collectionView{
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemModelCell", for: indexPath) as? UICollectionViewItemCell
        let model = self.dataSource[indexPath.row]
        itemCell?.setModel(model)
        
        if indexPath.row == self.dataSource.count - 2 {
            self.getData(nextPage: true)
        }
        
        return itemCell!
    }
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension CatalogViewController : PinterestLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        let model = self.dataSource[indexPath.row]
        print(CGFloat(model.imageHeight))
        if(model.imageHeight == 0){
            return 150
        }
        return CGFloat(model.imageHeight)
    }
}

extension CatalogViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.output.didSelectItem(dataSource[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if viewType == .tableView{
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "itemModelCell") as? UITableViewItemCell
        let model = self.dataSource[indexPath.row]
        itemCell?.setModel(model)
        
        if indexPath.row == self.dataSource.count - 2 {
            self.getData(nextPage: true)
        }
        
        return itemCell!
    }
}
