//
//  SearchView.swift
//  Takari
//
//  Created by yvp on 9/26/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SearchView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var logo: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit(){
//        self.addSubview(UINib(nibName: "SearchView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView)
        Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)
        self.contentView.backgroundColor = UIColor.red
        self.contentView.frame = self.bounds
        self.addSubview(contentView)
//        contentView.frame = self.bounds
//        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//        self.logo.layer.cornerRadius = self.logo.frame.height/2
    }

}
