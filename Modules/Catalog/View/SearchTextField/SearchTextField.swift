//
//  SearchTextField.swift
//  Takari
//
//  Created by ParaBellum on 9/18/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SearchTextField: UITextField {
    init(frame:CGRect, placeholderText:String, font: UIFont){
        super.init(frame: frame)
        self.leftViewMode = .always
        configureAppearance(placeholderText)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.leftViewMode = .always
        configureAppearance("Search...")
    }
    
    var tintedClearImage:UIImage?
    override func layoutSubviews() {
        super.layoutSubviews()
        tintClearImage()
    }
    private func tintClearImage(){
        for view in subviews {
            if view is UIButton{
                let button = view as! UIButton
                if let uiImage = button.image(for: .highlighted){
                    if tintedClearImage == nil{
                        tintedClearImage = tintImage(image:uiImage,color:tintColor)
                    }
                    button.setImage(tintedClearImage, for: .normal)
                    button.setImage(tintedClearImage, for: .highlighted)
                }
            }
        }
    }
    
    func tintImage(image:UIImage,color:UIColor) -> UIImage{
        let size = image.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        image.draw(at: CGPoint.zero, blendMode: CGBlendMode.normal, alpha: 1.0)
        
        context!.setFillColor(color.cgColor)
        context!.setBlendMode(CGBlendMode.sourceIn)
        context!.setAlpha(1.0)
        
        let rect = CGRect(x:0,y:0,width:image.size.width,height:image.size.height)
        UIGraphicsGetCurrentContext()!.fill(rect)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return tintedImage!
    }
    
    func configureAppearance(_ placeholderText:String){
        //Colors
        let color = UIColor(red: 200/255, green: 220/255, blue: 229/255, alpha: 1)
        self.textColor = color
        self.tintColor = color
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName:color])
        //BG
        self.backgroundColor = UIColor(red: 54/255, green: 134/255, blue: 171/255, alpha: 1)
        //LeftImage
        let image = #imageLiteral(resourceName: "ic_search_filters")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 10, y: 5, width: 18, height: 18)
        imageView.contentMode = .scaleAspectFit
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 30))
        paddingView.addSubview(imageView)
        self.leftView = paddingView
        self.leftViewMode = .always
        self.clearButtonMode = .whileEditing
        //Layer
        self.layer.cornerRadius = 5
        
    }
}
