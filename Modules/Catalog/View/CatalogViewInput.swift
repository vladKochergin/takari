//
//  CatalogCatalogViewInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol CatalogViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setFilter(filter: FilterModel)
    func setupInitialState()
    func reloadData(dataSource:Array<ItemModel>)
    
}
