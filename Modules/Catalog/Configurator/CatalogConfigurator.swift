//
//  CatalogCatalogConfigurator.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class CatalogModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? CatalogViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: CatalogViewController) {

        let router = CatalogRouter()

        let presenter = CatalogPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = CatalogInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
