//
//  CatalogCatalogInitializer.swift
//  Takari
//
//  Created by Vlad Kochergin on 19/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class CatalogModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var catalogViewController: CatalogViewController!

    override func awakeFromNib() {

        let configurator = CatalogModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: catalogViewController)
    }

}
