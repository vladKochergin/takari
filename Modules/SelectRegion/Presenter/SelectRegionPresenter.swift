//
//  SelectRegionSelectRegionPresenter.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

class SelectRegionPresenter: NSObject, SelectRegionModuleInput, SelectRegionViewOutput, SelectRegionInteractorOutput {

    weak var view: SelectRegionViewInput!
    var interactor: SelectRegionInteractorInput!
    var router: SelectRegionRouterInput!

    func viewIsReady() {
        self.view.setupInitialState()
    }
}
