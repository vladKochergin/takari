//
//  SelectRegionSelectRegionViewInput.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol SelectRegionViewInput: class {

    /**
        @author Vlad Kochergin
        Setup initial state of the view
    */

    func setupInitialState()
}
