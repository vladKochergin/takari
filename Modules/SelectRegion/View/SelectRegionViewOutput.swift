//
//  SelectRegionSelectRegionViewOutput.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

protocol SelectRegionViewOutput {

    /**
        @author Vlad Kochergin
        Notify presenter that view is ready
    */

    func viewIsReady()
}
