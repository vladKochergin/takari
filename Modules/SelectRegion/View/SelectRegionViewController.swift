//
//  SelectRegionSelectRegionViewController.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SelectRegionViewController: UITableViewController, SelectRegionViewInput {

    var output: SelectRegionViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }


    // MARK: SelectRegionViewInput
    func setupInitialState() {
    }
}
