//
//  SelectRegionSelectRegionConfigurator.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SelectRegionModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? SelectRegionViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: SelectRegionViewController) {

        let router = SelectRegionRouter()

        let presenter = SelectRegionPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SelectRegionInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter

        router.transitionHandler = viewController
    }

}
