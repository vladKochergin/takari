//
//  SelectRegionSelectRegionInitializer.swift
//  Takari
//
//  Created by Vlad Kochergin on 28/09/2017.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class SelectRegionModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var selectregionViewController: SelectRegionViewController!

    override func awakeFromNib() {

        let configurator = SelectRegionModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: selectregionViewController)
    }

}
