//
//  CategoryModel.swift
//  Takari
//
//  Created by yvp on 9/22/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class CategoryModel: Object {
    dynamic var categoryId = 0
    dynamic var parentId = 0
    dynamic var image = ""
    dynamic var title = ""
    dynamic var key = ""
    dynamic var itemsCount = 0
    dynamic var subsCount = 0
    dynamic var lvl = 0
    //    dynamic var dynamicProperty
    
    override static func primaryKey() -> String? {
        return "categoryId"
    }
    
    func parse(json: JSON){
        self.categoryId = json["id"].intValue
        self.parentId = json["pid"].intValue
        self.image = json["i"].stringValue
        self.title = json["t"].stringValue
        self.key = json["k"].stringValue
        self.itemsCount = json["items"].intValue
        self.subsCount = json["subs"].intValue
        self.lvl = json["lvl"].intValue
    }
}

