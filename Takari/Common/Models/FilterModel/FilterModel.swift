//
//  FilterModel.swift
//  Takari
//
//  Created by yvp on 9/26/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

enum FilterSortedType: String{
    case newest = "publicated_more"
    case cheap = "price_more"
    case expensive = "price_less"
    static let allValue = [newest, cheap, expensive]
}

enum FilterAdsType: String{
    case all = "publicated_more"
    case privateADS = "price_more"
    case business = "price_less"
    static let allValue = [all, privateADS, business]
}

class FilterModel: NSObject {
    
    var category: CategoryModel?
    var searchText: String = ""
    var dynamicProperty: Array<Any>?
    var sorted: FilterSortedType = .newest
    var adsType: FilterAdsType = .all
    
    func dictionary() -> Dictionary<String, String>{
        var dict = ["":""]
        if self.category != nil {
            dict["cat_up"] = self.category?.categoryId.description
        }
        
        if self.searchText.count > 0 {
            dict["title"] = self.searchText
        }
        
        return dict
    }
}
