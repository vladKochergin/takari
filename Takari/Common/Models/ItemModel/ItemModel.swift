//
//  ItemModel.swift
//  Takari
//
//  Created by yvp on 9/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import SwiftyJSON

class ItemModel : NSObject {
    
    var categoryName:String!
    var cityName:String?
    var createdDate:String?
    var itemDescription: String!
    var itemId:Int?
    var imageSmall:URL?
    var imageMedium:URL?
    var priceDisplay:String!
    var latitude:Double?
    var longitude:Double?
    var price:Double?
    var status:Int?
    var isFixed:Bool?
    var isMarked:Bool?
    var isPremium:Bool?
    var isQuick:Bool?
    var isTurbo:Bool?
    var isUp:Bool?
    var title:String?
//    var imageDataMedium: Data!
    var imageHeight: Double!
    
    init(json:JSON){
        self.categoryName   = json["cat_title"].stringValue
        self.cityName = json["city_title"].stringValue
//        self.createdDate = json[""]
        self.itemDescription = json["descr"].stringValue
        self.itemId = json["id"].intValue
        self.imageSmall = json["img_s"].url
        self.imageMedium = json["img_m"].url
        self.priceDisplay = json["item_price_display"].stringValue
        self.latitude = json["lat"].doubleValue
        self.longitude = json["lon"].doubleValue
        self.price = json["price"].doubleValue
        self.status = json["status"].intValue
        self.isFixed = json["svc_fixed"].boolValue
        self.isMarked = json["svc_marked"].boolValue
        self.isPremium = json["svc_premium"].boolValue
        self.isQuick = json["svc_quick"].boolValue
        self.isTurbo = json["svc_turbo"].boolValue
        self.isUp = json["svc_up"].boolValue
        self.title = json["title"].stringValue
        self.imageHeight = json["img_size"]["height"].doubleValue
    }
    
}
