//
//  UITableView+PullRequest.swift
//  Takari
//
//  Created by yvp on 9/19/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func reloadDataWithCheck(){
        self.reloadData()
        self.checkDataInTable()
        self.hideRefreshControl()
        self.hidePagingSpinner()
    }
    
    func addRefreshControlWithSelector(selector:Selector, view:UIViewController){
        let refreshControl: UIRefreshControl = UIRefreshControl()
        refreshControl .addTarget(view, action: selector, for: .valueChanged)
        refreshControl.layer.zPosition = -1
        self.addSubview(refreshControl)
    }
    
    func showRefreshControl(){
        if (self.subviews.last?.responds(to: #selector(UIRefreshControl.beginRefreshing)))!{
            (self.subviews.last as! UIRefreshControl).beginRefreshing()
        }
    }
    
    func hideRefreshControl(){
        for view in self.subviews {
            if view.isKind(of: UIRefreshControl.self) {
                (view as! UIRefreshControl).endRefreshing()
            }
        }
    }
    
    func showPagingSpinner(){
        let pgSpinner: UIView = UIView(frame: CGRect(x: 0.0, y: self.bounds.height-50, width: UIScreen.main.bounds.width, height: 40.0))
        pgSpinner.restorationIdentifier = "pgSpinner"
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actInd.tag = 10
        actInd.frame = CGRect(x: UIScreen.main.bounds.width/2, y: 5.0, width: 20.0, height: 20.0)
        actInd.hidesWhenStopped = true
        actInd.startAnimating()
        pgSpinner.addSubview(actInd)
        self.addSubview(pgSpinner)
    }
    
    func hidePagingSpinner(){
        for view in self.subviews {
            if view.restorationIdentifier == "pgSpinner" {
                view.removeFromSuperview()
            }
        }
    }
    
    func isSpinnerShow(){
        
    }
    
    func checkDataInTable(){
        if (self.numberOfItems(inSection: 0) == 0) {
            let label: UILabel = UILabel(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
            label.text = "No Data"
            label.tag = 444
            self.addSubview(label)
        } else {
            for view in self.subviews{
                if view.tag == 444 {
                    view.removeFromSuperview()
                }
            }
            
        }
    }
}

