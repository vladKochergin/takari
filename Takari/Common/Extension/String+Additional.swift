//
//  String+Localized.swift
//  Takari
//
//  Created by yvp on 9/15/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
