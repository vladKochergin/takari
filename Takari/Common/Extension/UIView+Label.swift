//
//  UIView+Label.swift
//  Takari
//
//  Created by yvp on 9/21/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    func viewPath(distance: CGFloat, width: CGFloat, text: String) {
        let labelView: LabelView = LabelView(frame: self.frame)
        labelView.setLabel(distance: distance, width: width, text: text)
        self.addSubview(labelView)
    }
    
    func removeLabelView() {
        for view in self.subviews{
            if view.isKind(of: LabelView.self) {
                view.removeFromSuperview()
//                (view as! LabelView).removeLabel()
            }
        }
    }
}


