//
//  RequestMapManager.swift
//  Takari
//
//  Created by yvp on 9/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias MappingCompletion = (_ array: Array<Any>?, _ dictionary: Dictionary<String, Any>?, _ object: Any) -> Void

class RequestMapManager: NSObject {
    
    func TEST(param:Dictionary<String, Any>, param2:Dictionary<String, Any>) -> Void {
        
    }
    
    class func getCatalogLists(parameters:Dictionary<String, Any>, completion:@escaping MappingCompletion) -> Void {
        WebService.sharedInstance.getCatalogList(parameters) { (array, json, error) in
            if error != nil {
                error?.showErrorMessage()
                return
            }
            let itemsList = json!["items_list"]["rows"].arrayValue //dict?["items_list"] as! NSDictionary
            //            let itemsArray = itemsList["rows"] as! [NSDictionary]
            var itemArray:[ItemModel] = []
            for dict in itemsList{
                let model:ItemModel = ItemModel(json: dict)
                itemArray.append(model)
            }
            completion(itemArray, nil, Any.self)
        }
    }
    class func getDetailItem(itemID: Int, completion:@escaping MappingCompletion)-> Void{
        WebService.sharedInstance.getDetailItem(itemID) { (array, json, error) in
            if error != nil{
                error?.showErrorMessage()
                return
            }
            let itemInfo = json!["item_info"]
            let itemDetailsModel = ItemDetails(JSON: itemInfo)
            completion(nil, nil, itemDetailsModel)
        }
    }
    
    class func getCategoryTree(completion:@escaping MappingCompletion) {
        WebService.sharedInstance.getCategoryTree { (array, json, error) in
            if error != nil{
                error?.showErrorMessage()
                return
            }
            let itemsList = json!["cats_tree"]["rows"].arrayValue
            //            DispatchQueue.global(qos: .userInitiated).async {
            for dict in itemsList {
                let categoryModel = CategoryModel()
                categoryModel.parse(json: dict)
                RealmService.saveCategory(categoryModel: categoryModel)
            }
            //            }
            completion(nil, nil, (Any).self)
        }
    }
}

