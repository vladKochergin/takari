//
//  WebService.swift
//  Takari
//
//  Created by yvp on 9/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias RequestCompletion = (_ array: Array<Any>?, _ json:JSON?, _ error:ErrorHandler?) -> Void

class WebService: SessionManager{
    
    static let sharedInstance : WebService = {
        let instance = WebService()
        return instance
    }()
    
    private let baseURL = "http://takarieu.iskytest.com/api"
    var loginToken:NSString!
    
    func getCatalogList(_ parameters:Dictionary<String, Any>, completion:@escaping RequestCompletion) {
        request(baseURL.appending("/bbs"), method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON {(response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                completion(nil, JSON(response.result.value as! NSDictionary), self.checkStatus(response.value as! [String:Any]))
            case .failure(_):
                completion(nil, nil, ErrorHandler(systemError:response.result.error!))
            }
        }
    }
    
    func getDetailItem(_ itemId: Int, completion:@escaping RequestCompletion){
        let parameters = ["item_info":["id": String(itemId),
                                       "similar": "1",
                                       "need_cat_dyn_info": "1"]
        ]
        request(baseURL.appending("/bbs"), method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON {(response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                completion(nil, JSON(response.result.value as! NSDictionary), self.checkStatus(response.value as! [String:Any]))
            case .failure(_):
                completion(nil, nil, ErrorHandler(systemError:response.result.error!))
            }
        }
    }
    
    // MARK: - Category
    
    func getCategoryTree(completion:@escaping RequestCompletion) {
        let parameters = ["get_tree_cats":[:]]
        request(baseURL.appending("/cats"), method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON {(response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                completion(nil, JSON(response.result.value as! NSDictionary), self.checkStatus(response.value as! [String:Any]))
            case .failure(_):
                completion(nil, nil, ErrorHandler(systemError:response.result.error!))
            }
        }
    }
    
    // MARK: - Error Handler
    
    func checkStatus(_ response:[String:Any]) -> ErrorHandler? {
        if (response["RESULT"] as! Int) == 0 && response.keys.contains("MESSAGE"){
            let arr: Array = (response["MESSAGE"] as! Array<Any>)
            let dict = arr.first as! Dictionary<String, Any>
            let stringError:String = dict["msg"] as! String
            let error:ErrorHandler = ErrorHandler(stringError)
            return error
        }
        
        if (response["RESULT"] as! Int) == 0 {
            return ErrorHandler("UNKNOWN_ERROR")
        }
        
        return nil
    }
}
