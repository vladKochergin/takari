//
//  ErrorHandler.swift
//  Takari
//
//  Created by yvp on 9/14/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import KVAlert

class ErrorHandler: Error {
    let message: String
    
//    public var localizedDescription: String {
//        return message
//    }
    
    init(_ message: String) {
        self.message = message.localized
    }
    
    init(systemError error: Error) {
        self.message = error.localizedDescription
    }
    
    func showErrorMessage(){
        //WARNING: MBProgressHUD (HIDE)
        KVAlert.alert(withTitle: "Error", message: self.message, preferredStyle: .alert, completion:nil)
    }
}
