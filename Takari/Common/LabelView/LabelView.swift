//
//  LabelView.swift
//  Takari
//
//  Created by yvp on 9/21/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit

class LabelView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLabel(distance: CGFloat, width: CGFloat, text: String){
        let viewPath: UIBezierPath = UIBezierPath()
        
        viewPath.move(to: CGPoint(x: 0, y: distance))
        viewPath.addLine(to: CGPoint(x: 0, y: distance+width))
        viewPath.addLine(to: CGPoint(x: distance+width, y: 0))
        viewPath.addLine(to: CGPoint(x: distance, y: 0))
        viewPath.addLine(to: CGPoint(x: 0, y: distance))
        viewPath.close()
        
        let borderLayer: CAShapeLayer = CAShapeLayer()
        borderLayer.path = viewPath.cgPath
        borderLayer.fillColor = UIColor(red: 248/255, green: 186/255, blue: 65/255, alpha: 1).cgColor
        self.layer.addSublayer(borderLayer)
        self.addSubview(self.getLabel(distance: distance, width: width, text: text))
    }

    private func getLabel(distance: CGFloat, width: CGFloat, text: String) -> UILabel{
        //        let ly: Int = (Int(distance)+width)/2
        let size: Int = Int(sqrt(pow(distance,2) + pow(distance,2)))//sqrt(pow(distance)+pow(distance,2))
        //        let label = UILabel(frame: CGRect(x: lx/2, y: ly - width, width: 70, height: width))
        let lx: Int = (Int(distance)/2)+Int(width)/3
        let ly: Int = lx //(Int(distance) - width)/2
        let label = UILabel(frame: CGRect(x: lx - size/2, y: ly - Int(width)/3, width: size, height: Int(width)/3))
        label.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi/4));
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textAlignment = .center
        label.text = text
        label.tag = 1
        return label
    }
    
    func removeLabel(){
        self.removeFromSuperview()
    }
}
