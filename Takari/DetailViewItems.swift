//
//  DetailViewPictureItem.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//
protocol DetailViewModelItem{
    var type: ItemDetailsViewModelItemType {get}
}

enum ItemDetailsViewModelItemType{
    case image
    case desctription
    case internals
    case map
    case similarAd
    case views
}

class DetailViewModelImageItem: DetailViewModelItem{
    var type: ItemDetailsViewModelItemType{
        return .image
    }
    var title:String
    var priceDisplay:String
    var dateCreated:String
    
    var pictureURLs:[URL]
    init(pictureURLs:[URL],title:String,priceDisplay:String,dateCreated:String){
        self.pictureURLs = pictureURLs
        self.title = title
        self.dateCreated = dateCreated
        self.priceDisplay = priceDisplay
    }
    
}

class DetailViewModelViewsItem: DetailViewModelItem{
    var type:ItemDetailsViewModelItemType{
        return .views
    }
    var numberOfView:Int
    var adID:Int
    init(numOfViews:Int,ID:Int){
        self.numberOfView = numOfViews
        self.adID = ID
    }
}

class DetailViewModelDescriptionItem: DetailViewModelItem{
    var type: ItemDetailsViewModelItemType{
        return .desctription
    }
    var detailDescription:String!
    init(itemDescription:String) {
        self.detailDescription = itemDescription
    }
}

class DetailViewModelInternalsItem: DetailViewModelItem{
    var type: ItemDetailsViewModelItemType{
        return .internals
    }
    var internals:Internals
    init(internals:Internals) {
        self.internals = internals
    }
}

class DetailViewModelMapItem: DetailViewModelItem{
    var type: ItemDetailsViewModelItemType{
        return .map
    }
    var detailLongitude:Double?
    var detailLatitude:Double?
    var user:UserModel!
    var cityTitle:String!

    init(longitude:Double,latitude:Double,user:UserModel,cityTitle:String){
        
        self.detailLongitude = longitude == 0.0 ? nil:longitude
        self.detailLatitude = latitude == 0.0 ? nil:latitude
        self.cityTitle = cityTitle
        self.user = user
        
    }
}

class DetailViewModelSimilarAdItem: DetailViewModelItem{
    var type: ItemDetailsViewModelItemType{
        return .similarAd
    }
    var similarADs:[ItemModel]
    init(similarAds:[ItemModel]) {
        self.similarADs = similarAds
    }
}


