//
//  RealmService.swift
//  Takari
//
//  Created by yvp on 9/25/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import RealmSwift

class RealmService: NSObject {
    
    class func saveCategory(categoryModel: CategoryModel) {
        let realm = try! Realm()
        try! realm.write {
            realm.create(CategoryModel.self, value: categoryModel, update: true)
        }
    }
    class func getCategory(lvl: Int, parentId: Int) -> Array<CategoryModel> {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "lvl = %i AND parentId = %i", lvl, parentId)
        let result = realm.objects(CategoryModel.self).filter(predicate)
        return Array(result)
    }
}
