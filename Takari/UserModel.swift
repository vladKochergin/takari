//
//  UserModel.swift
//  Takari
//
//  Created by ParaBellum on 9/25/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserModel:NSObject{
    var name:String!
    var surname:String!
    var id: Int!
    var email:String?
    var skype:String?
    var phoneNumber:String!
    var login:String!
    var avatar:String?
    var sex:String!
    
    
    init(json:JSON){
        self.name = json["name"].stringValue
        self.surname = json["surname"].stringValue
        self.id = json["id"].intValue
        self.email = json["email"].stringValue
        self.skype = json["skype"].stringValue
        self.phoneNumber = json["phone_number"].stringValue
        self.login = json["login"].stringValue
        self.avatar = json["avatar"].stringValue
        self.sex = json["sex"].stringValue
        
        
    }
}
