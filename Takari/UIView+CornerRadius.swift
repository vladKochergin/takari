//
//  UIView+CornerRadius.swift
//  Takari
//
//  Created by ParaBellum on 9/25/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
@IBDesignable
class UIView_CornerRadius: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
}
