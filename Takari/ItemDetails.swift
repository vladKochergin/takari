//
//  TestItemDetails.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
import SwiftyJSON
class ItemDetails:NSObject{
    
    var title:String!
    var itemDescription:String!
    var price:Double
    var dynamicInternals:[Internals] = []
    var priceDisplay:String!
    var imageURLs:[URL] = []
    var similarAds:[ItemModel] = []
    var phones:[String] = []
    var image_m: URL?
    var createdDate:String!
    var numberOfView:Int!
    var adID:Int!
    //Map
    var user:UserModel!
    var addrLat:Double!
    var addrLon:Double!
    var cityTitle:String!
    
    
    init(JSON:JSON){
        self.title = JSON["title"].stringValue
        self.itemDescription = JSON["descr"].stringValue
        self.price = JSON["price"].doubleValue
        self.priceDisplay = JSON["item_price_display"].stringValue
        self.createdDate = JSON["created"].stringValue
        self.numberOfView = JSON["views_total"].intValue
        self.adID = JSON["id"].intValue
        
        //Map--------------------------------------------
        self.user = UserModel(json:JSON["user"])
        self.addrLat = JSON["addr_lat"].doubleValue
        self.addrLon = JSON["addr_lon"].doubleValue
        self.cityTitle = JSON["city_title"].stringValue
        //------------------------------------------------
        let imageURLsArray = JSON["images"].arrayValue
        for image in imageURLsArray{
            let imageURL = image["path"].url
            if let imgURL = imageURL{
                self.imageURLs.append(imgURL)
            }
        }
        let similarRows = JSON["similar"]["rows"].arrayValue
        for similarAd in similarRows{
            let model = ItemModel(json: similarAd)
            self.similarAds.append(model)
        }
        let phonesArray = JSON["phones"].arrayValue
        for phoneNumber in phonesArray{
            let number = phoneNumber["v"].stringValue
            self.phones.append(number)
            
        }
        self.image_m = JSON["img_m"].url
        
    
      //  let catProps = JSON["cat_props"] as! [String:Any]
//        let array = Array(catProps.values)
       // print(catProps)
////        let catProsArray = Array(catProps.values) as! [[String:Any]]
  
    }

}





















