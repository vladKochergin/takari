//
//  Internals.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
class Internals: NSObject{
    var name:String
    var value:String

    init(JSON:JSON){
        self.name = JSON["title"].stringValue
        self.value = JSON["v"].stringValue
    }
}
