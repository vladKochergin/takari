//
//  DetailViewModel.swift
//  Takari
//
//  Created by ParaBellum on 9/20/17.
//  Copyright © 2017 iSky.Solutions. All rights reserved.
//

import UIKit
class DetailViewModel: NSObject {
    var items = [DetailViewModelItem]()
    init(with details:ItemDetails){
        
        //Pictures
        let imageItem = DetailViewModelImageItem(pictureURLs: details.imageURLs, title: details.title, priceDisplay: details.priceDisplay, dateCreated: details.createdDate)
        items.append(imageItem)
        
        //Internals
        //TEST INTERNALS
        let internalItem = DetailViewModelInternalsItem(internals: Internals(JSON: ["title":"Test title","v":"TestValue"]))
        items.append(internalItem)
        let internalItem2 = DetailViewModelInternalsItem(internals: Internals(JSON: ["title":"Test title 2","v":"20000"]))
        items.append(internalItem2)
        //        if !details.dynamicInternals.isEmpty {
        //            for internalItem in details.dynamicInternals{
        //                let iternItem = DetailViewModelInternalsItem(internals: internalItem)
        //                items.append(iternItem)
        //
        //            }
        //        }
        
        //Description
        let descr = DetailViewModelDescriptionItem(itemDescription: details.itemDescription)
        items.append(descr)
        //Map
        let mapItem = DetailViewModelMapItem(longitude: details.addrLon, latitude: details.addrLat, user: details.user, cityTitle: details.cityTitle)
        self.items.append(mapItem)
        
        //SimilarADs
        let itemAd = DetailViewModelSimilarAdItem(similarAds: details.similarAds)
        self.items.append(itemAd)
        
        
        //LastRow
        let viewsItem = DetailViewModelViewsItem(numOfViews: details.numberOfView, ID: details.adID)
        self.items.append(viewsItem)
    }
}
